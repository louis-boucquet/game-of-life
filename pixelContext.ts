function getCanvas(x: number, y: number): CanvasRenderingContext2D {
	const canvas = document.createElement("canvas");
	canvas.width = x;
	canvas.height = y;

	document.body.appendChild(canvas);

	return canvas.getContext("2d") as any;
}

type PixelRenderer = (x: number, y: number) => string;

function getPixelContext(x: number, y: number, renderer: PixelRenderer) {
	const ctx = getCanvas(x, y);
	ctx.imageSmoothingEnabled = false;

	return {
		setRenderer(newRenderer: PixelRenderer) {
			renderer = newRenderer;
		},
		render() {
			for (let xPixel = 0; xPixel < x; xPixel++) {
				for (let yPixel = 0; yPixel < y; yPixel++) {
					ctx.fillStyle = renderer(xPixel, yPixel);
					ctx.fillRect(xPixel, yPixel, 1, 1);
				}
			}
		}
	}
}
