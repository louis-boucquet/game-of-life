type Color = {
	r: number,
	g: number,
	b: number,
}

type Gradient = (postition: number) => Color;

type GradientPoint = {
	color: Color,
	position: number
}

function getColor(...numbers: number[]) {
	return `rgb(${numbers
		.map(n => n * 255)
		.map(Math.floor)
		.join(",")})`;
}

function getColorString({ r, g, b }: Color) {
	return getColor(r, g, b);
}

function getGradient(points: GradientPoint[]): Gradient {
	function gradientTwo(
		from: Color,
		to: Color,
		amount: number
	): Color {
		return {
			r: from.r * (1 - amount) + to.r * amount,
			g: from.g * (1 - amount) + to.g * amount,
			b: from.b * (1 - amount) + to.b * amount,
		}
	}

	return gradientPosition => {

		let [start, ...rest] = points;

		for (const next of rest) {
			if (gradientPosition < next.position) {
				return gradientTwo(
					start.color,
					next.color,
					(gradientPosition - start.position) / (next.position - start.position)
				)
			}

			start = next;
		}

		return {
			r: 0,
			g: 0,
			b: 0,
		};
	}
}