class Coordinate {
	constructor(
		readonly x: number,
		readonly y: number,
	) { }

	static random({ x, y }: Coordinate) {
		return new Coordinate(
			Math.floor(Math.random() * x),
			Math.floor(Math.random() * y)
		)
	}

	static checkBound({ x, y }: Coordinate, { x: xB, y: yB }: Coordinate) {
		return x >= 0 && y >= 0 && x < xB && y < yB;
	}

	static surrounding(from: Coordinate) {
		return [
			new Coordinate(-1, -1),
			new Coordinate(0, -1),
			new Coordinate(1, -1),

			new Coordinate(-1, 0),
			new Coordinate(1, 0),

			new Coordinate(-1, 1),
			new Coordinate(0, 1),
			new Coordinate(1, 1),
		].map(toAdd => Coordinate.add(toAdd, from));
	}

	static add({ x: x1, y: y1 }: Coordinate, { x: x2, y: y2 }: Coordinate): Coordinate {
		return new Coordinate(x1 + x2, y1 + y2);
	}
}