const pixelSize = 10;

const xCanvas = Math.floor(window.innerWidth / pixelSize);
const yCanvas = Math.floor(window.innerHeight / pixelSize);

// const xCanvas = 10;
// const yCanvas = 10;

const fade = 0.99;

const grid = getGrid(xCanvas, yCanvas, (x, y) => 0);
const game = GameOfLife(xCanvas, yCanvas);
game.birthRandom(0.1);

const gradient = getGradient([
	{
		position: 0,
		color: { r: 1, g: 0.7, b: 1 }
	},
	{
		position: 0.01,
		color: { r: 1, g: 0.5, b: 1 }
	},
	{
		position: 0.1,
		color: { r: 1, g: 0, b: 1 }
	},
	{
		position: 0.2,
		color: { r: 0.4, g: 0, b: 1 }
	},
	{
		position: 1,
		color: { r: 0, g: 0, b: 0 }
	}
])

function conwayRenderer(pixelX: number, pixelY: number) {
	if (game.grid[pixelY + 0][pixelX + 0])
		grid[pixelY][pixelX] = 1;
	else
		grid[pixelY][pixelX] *= fade;

	const brightness = grid[pixelY][pixelX];

	const gradientPosition = 1 - brightness;

	return getColorString(gradient(gradientPosition));
}

const pixelContext = getPixelContext(xCanvas, yCanvas, conwayRenderer);

function update() {
	game.birthRandom(0.001);
	game.update();
	pixelContext.render();

	requestAnimationFrame(update);
}

update();

function getGrid<T>(xGrid: number, yGrid: number, value: (x: number, y: number) => T) {
	const grid: T[][] = [];

	for (let y = 0; y < yGrid; y++) {
		const row: T[] = [];

		for (let x = 0; x < xGrid; x++)
			row.push(value(x, y));

		grid.push(row);
	}

	return grid;
}
