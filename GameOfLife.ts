function GameOfLife(xGrid: number, yGrid: number) {
	const grid = getGrid(xGrid, yGrid, () => 0);
	const size = new Coordinate(xGrid, yGrid);

	window.addEventListener("click", e => {
		const x = Math.floor(e.clientX / pixelSize);
		const y = Math.floor(e.clientY / pixelSize);

		grid[y][x] = 1;
	});

	return {
		update() {
			const toKill = new Set<Coordinate>();
			const toBirth = new Set<Coordinate>();

			for (let y = 0; y < yGrid; y++) {
				for (let x = 0; x < xGrid; x++) {
					const neighbours = Coordinate
							.surrounding(new Coordinate(x, y))
							.filter(c => Coordinate.checkBound(c, size))
							.reduce((tot, { x, y }) => tot + grid[y][x], 0);

					if (grid[y][x]) {
						if (!(neighbours === 2 || neighbours === 3))
							toKill.add(new Coordinate(x, y));
					} else {
						if (neighbours === 3)
							toBirth.add(new Coordinate(x, y));
					}
				}
			}

			toKill.forEach(({ x, y }) => grid[y][x] = 0);
			toBirth.forEach(({ x, y }) => grid[y][x] = 1);
		},
		birthRandom(ratio: number) {
			for (let _ = 0; _ < xGrid * yGrid * ratio; _++) {
				const { x, y } = Coordinate.random(size);

				grid[y][x] = 1;
			}
		},
		get grid() {
			return grid;
		}
	}
}
